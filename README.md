# 资源管理系统

# 在线文档
https://www.showdoc.com.cn/1539341513122829/8089082317099646

## 说明

资源管理系统，是一款集中前后端配套的资源管理系统。集成文档，题库，图片，资讯，文章等多功能的场景式系统管理，能适应多种业务场景需求。
采用前后端分离全栈开发，模块化开发。前端模版式开发，社区正在积极建设开发中，欢迎开发者入驻开发模版及插件。

## 技术栈

- vue
- php
- nodejs
- 模块化
- thinkphp
- vue-cli
- mysql

## 目录结构

```
├─application 接口目录
│ ├─index 首页接口目录
│ ├─admin 管理员后台接口目录
│ ├─user 用户接口目录
│ ├─pay 支付接口目录
│ ├─token token 验证接口目录
│ ├─api 公共接口目录
│ └─route.php 路由配置文件
├─admin 后台管理文件目录
│
├─upload 文件上传目录
│ ├─image 图片保存目录
│ ├─logo logo 图片保存目录
│ ├─other 文件保存目录
│ ├─slide 幻灯片保存目录
│ ├─tmp 临时存放目录——可以定期删除,节省空间
│
│
├─template 前端模版目录
│ ├─m1 模版一
│ └─ ....
├─tool 工具目录
│
├─public WEB 目录（对外接口访问目录）
│ ├─index.php 入口文件
│ ├─router.php 快速测试文件
│ ├─install 系统程序安装引导文件目录
│ │ ├─config.db.php 系统数据库配置文件(修改数据库信息在这里修改)
│ │ └─install.lock 安装锁文件(需要重新安装程序时删除该文件和 config.db.php 文件即可)
│
├─thinkphp thinkphp 核心类库源码目录
│
├─vendor 第三方类库文件目录
├─extend 扩展类库目录
├─runtime 应用的运行时目录（可写，可定制）
├─build.php 自动生成定义文件（参考）
├─composer.json composer 定义文件
├─LICENSE.txt 授权说明文件
├─README.md README 文件
├─index.phhp 项目入口文件
```

## 新添功能特性

- 模版化管理
- 文档管理
- 图库管理
- 题库管理
- 资讯管理

## 体验地址

[体验地址](http://zy.xskj.store)

## 项目截图

- 首页:![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/200033_14bfd0cc_7358515.png "屏幕截图.png")
- 后台:![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/195824_326b74b2_7358515.png "屏幕截图.png")

## 下载地址----请下载新版本

- v1.0 程序下载:[下载](https://fusong.lanzous.com/b01c4sc8b)密码:69kz
- v2.0 程序下载:[下载](https://fusong.lanzous.com/b01c9c2gd)密码:a0ym
- v2.0.1 程序下载: [下载](https://fusong.lanzoui.com/iicr5qtgegb)
- v2.1.. 程序下载: [下载](https://fusong.lanzoui.com/iagPTqx8fuh)
- v3.1.0 程序下载: [下载](https://fusong.lanzoui.com/iLbLHsvnibg)
- v5 程序下载 [下载](https://fusong.lanzoui.com/b01cp062h)密码:38lh
- 资源包:[下载](https://fusong.lanzous.com/b01c4vzkj)密码:6hvm

### 环境

- php>7.0(建议 php7.4)
- 安装相关拓展:
- 给 template 目录读写权限

### 安装方法

- 把源码包解压放到网站跟目录，然后访问域名即可进入安装页面。根据页面操作填写数据库信息即可。
  注意:_如果安装后发现进入数据库中查看没有 xs_user 表，将/public/install/localhost.sql 自己导入到相应数据即可_
- 后台地址为:http://域名/public/index.php/admin
  (默认管理员账号:admin,密码:xskj,请安装装后自行修改管理员信息)

## 日志更新

- 2021-4-03 修复导入 excel 文件 bug
- 2021-4-06 添加微信公众号查题接口，修复后台管理登陆 bug,修复 logo 上传,幻灯片上传 bug,优化数据库表结构。
- 2021-4-06 修复微信公众号接口 bug,支持文本，图片，链接，地图接口功能，
- 2021-6-29 整合优化了数据表，修复了查题 bug
- 2021-7-02 添加驾校题库，中小学题库等 40 万(由于我也是花钱买的，所以此资源付费，需要的联系我)
- 2021-8-19 全新架构 v3.0
- 2021-8-27 修复 user 表 bug, 添加微信公众号部分功能， 优化代码结构，添加 tesseract-ocr 图像文字识别提起，具体安装步骤见官方文档
- 2021-9-7 添加百度通用文字识别，聚合数据识别功能整合--对应版本 v5.0.0
- 2021-10-8 修复后台图片文档删除接口 bug -----删除不彻底问题
- 2021-10-16 修复前端模版文档阅览问题  添加新的阅览接口 感谢文档开源软件kkFileView  这里的接口默认使用我自己搭建的文档阅览接口，如果需要自己用kkFileView搭建属于自己的文档阅览接口，请访问https://kkfileview.keking.cn/zh-cn/index.html  接口免费供大家使用，但请大家珍惜，不要非法频繁调用
- 2021-10-16 修复公众号文档阅览不了问题， 请大家升级至v5.0.4.
- 2021.12.1 发布v6版本
## 问题解决

- 关于微信公众号对接显示参数错误的原因
  由于微信服务器的限制，可能你的域名未备案等原因导致你的域名被微信服务器封。解决办法用 ip 替换域名进行对接。亲测有效

## 捐赠

- 项目开发不易，欢迎老板捐赠，给点动力，嘻嘻
  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0827/033202_881ce747_7358515.jpeg "1630006085911.jpg")
  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0827/033215_a04c3d69_7358515.png "mm_facetoface_collect_qrcode_1630006064511.png")
