<?php
namespace app\index\controller;

use think\Controller;
use think\Db;


class News extends Controller
{




    // 点赞接口
    public function newsStar()
    {

        $id = input("get.id");

        $star = Db::name("news")->where("id", $id)->find();
        $re = Db::name("news")->where("id", $id)->update([
            "star" => (int)$star["star"] + 1
        ]);

        if (!empty($re)) {
            return 1;
        }
        else {
            return 0;
        }
    }


    /**
     * 文章搜索接口
     * @return void
     */
    public function searchNews()
    {


        $content = input("get.keyword");

        $data = Db::name("news")->where("title", "like", "%" . $content . "%")->limit(10)->select();


        $data = array(
            "code" => 200,
            "result" => $data
        );
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }



    //文章详情页接口       
    public function getNewsDetail()
    {
        /*
         *请求参数:
         *@param id int 文章id
         */
        $id = input("get.id");
        $keyword = input("");
        //阅读量递增
        $hits = Db::name("news")->where("id", $id)->find();

        $re = Db::name("news")->where("id", $id)->update([
            "hits" => (int)$hits["hits"] + 1
        ]);

        $data = Db::name("news")->where("id", $id)->find();
        //dump($data);
        //返回数据        
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }


    // 请求资讯列表接口
    public function getNewsList()
    {

        //参数接收
        $page = (int)input('get.page'); //页数
        $eachPageNum = (int)input('get.eachPageNum'); //每页显示数量
        $search = input('get.search');


        if ($search == "全部") {
            // 没有搜索内容
            $data = Db::name('news')
                ->order('id desc')
                ->page($page, $eachPageNum)
                ->field('id,title,time, author, hits, star, status, label, category, image_url')
                ->select();
        }
        else {
            //有搜索内容
            $data = Db::name('news')
                ->where("title", "like", $search)
                ->order('id desc')
                ->page($page, $eachPageNum)
                ->field('id,title,time, author, hits, star, status, label, category, image_url')
                ->select();
        }

        $count = Db::name('news')->count();

        $data = array(
            "code" => 200,
            "description" => "这是前端资讯列表请求信息",
            "data" => $data, //分页数据
            "count" => $count //数量
        );
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }

}