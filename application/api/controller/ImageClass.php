<?php

namespace app\api\controller;

use think\Controller;

/**
 * 图像处理类操作：链式操作关键在于调用函数后返回该实例化类的指针
 *@param string  $path  本地绝对地址
 */
class ImageClass extends Controller
{

    private $image;
    //图像的初始化
    function __construct($path)
    {
        //实例化一个图像对象
        $this->image = \think\Image::open($path);
    }


    /*
     *获取图像的参数信息
     * get
     *@return [] $config
     */
    private function getImageConfig()
    {
        $config = [];
        //图片的宽度
        $config["width"] = $this->image->width();
        //图片的高度
        $config["height"] = $this->image->height();
        //图片的类型
        $config["type"] = $this->image->type();
        //图片的mime类型
        $config["mime"] = $this->image->mime();
        // 返回图片的尺寸数组 0 图片宽度 1 图片高度       
        $config["size"] = $this->image->size();

        return $config;
    }


    /*
     *裁剪图像
     * set
     *@return $this
     */
    private function crop()
    {
        //获取裁剪的大小尺寸
        $size = func_get_args();
        if (count($size) <= 2) {
            //默认裁剪位置
            $size[2] = 0;
            $size[3] = 0;
        }
        //注意这里两个image对象不一样！！！！！！
        $this->image = $this->imgae->crop($size[0], $size[1], $size[2], $size[3]);

        return $this;
    }


    /*
     *生成缩略图像
     * set
     *@return $this
     */
    private function thumb()
    {
        // 常量，标识缩略图等比例缩放类型
// const THUMB_SCALING   = 1; 
// 常量，标识缩略图缩放后填充类型
// const THUMB_FILLED    = 2; 
// 量，标识缩略图居中裁剪类型
// const THUMB_CENTER    = 3; 
// 常量，标识缩略图左上角裁剪类型
// const THUMB_NORTHWEST = 4;
// 常量，标识缩略图右下角裁剪类型
// const THUMB_SOUTHEAST = 5; 
// 常量，标识缩略图固定尺寸缩放类型
// const THUMB_FIXED     = 6; 

        //获取裁剪的大小尺寸
        $size = func_get_args();
        if (count($size) == 0) {
            //默认最大为150*150缩略图
            $size[0] = 150;
            $size[1] = 150;
            //默认居中裁剪缩略
            $size[2] = \think\Image::THUMB_CENTER;
        }
        //注意这里两个image对象不一样！！！！！！
        $this->image = $this->imgae->thumb($size[0], $size[1], $size[2]);

        return $this;
    }


    /*
     *添加水印
     * set
     * retrun $this
     */

    private function water($path, $position = \think\Image::WATER_SOUTHEAST)
    {

        $this->image = $this->image->water($path, $position);
        return $this;

    }


    /*
     *添加文字
     * set
     * return $this
     */

    private function text($text, $font, $size, $color = '#00000000', $locate = \think\Image::WATER_SOUTHEAST, $offset = 0, $angle = 0)
    {

        $this->image = $this->image->text($text, $font, $size, $color, $locate, $offset, $angle);
        return $this;
    }


    /*
     *保存图片
     * set
     * return $this
     */

    private function save($pathname, $type = null, $quality = 80, $interlace = true)
    {

        $this->image = $this->image->save($pathname,
            $type,
            $quality,
            $interlace);
        return $this;
    }
}