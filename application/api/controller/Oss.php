<?php
namespace app\api\controller;
use think\Controller;
use think\Db;
use OSS\OssClient;
use OSS\Core\OssException;


class Oss extends Controller
{

    //初始化云存储账号
    public function __construct()
    {
        //获取数据库信息
        $config = Db::name('oss')->find();

        //初始化阿里云oss存储账号信息
        $this->aliOss = array(
            "accessKeyId" => $config["aliAccessKeyId"],
            "accessKeySecret" => $config["aliAccessKeySecret"],
            // Endpoint以杭州为例，其它Region请按实际情况填写。        
            "endpoint" => $config["aliEndpoint"],
            // 存储空间名称        
            "bucket" => $config["aliBucket"]
        );
        //实例化阿里云oss存储对象
        $this->ossClient = new OssClient($this->aliOss["accessKeyId"], $this->aliOss["accessKeySecret"], $this->aliOss["endpoint"]);

        //创建Bucket存储空间
        $this->aliCreateBucket();
    }




    /*
     *阿里oss存储空间bucket创建接口
     *@param  null
     *return  array  $data          返回状态  0-失败 1——失败
     */

    public function aliCreateBucket()
    {
        //判断bucket是否存在
        if (!$this->ossClient->doesBucketExist($this->aliOss["bucket"])) {
            //不存在创建bucket
            //设置存储空间的存储类型为低频访问类型，默认是标准类型
            $options = array(
                OssClient::OSS_STORAGE => OssClient::OSS_STORAGE_IA
            );
            try {
                // 设置存储空间的权限为公共读，默认是私有读写。
                $this->ossClient->createBucket($this->aliOss["bucket"], OssClient::OSS_ACL_TYPE_PUBLIC_READ, $options);
                $data = array(
                    "code" => 1,
                    "msg" => "success"
                );
            }
            catch (OssException $e) {
                $data = array(
                    "code" => 0,
                    "msg" => $e->getMessage()
                );
            }
            return $data;
        }
    }


    /*
     *阿里文件上传云存储接口
     *@param  string $description  文件描述
     *@param  string $path        文件路径
     *return  array  $data          返回状态  0-失败 1——失败
     */
    public function aliUpload($path, $description)
    {
        // <yourObjectName>上传文件到OSS时需要指定包含文件后缀在内的完整路径，例如abc/efg/123.jpg        
        $object = $path;
        $content = $description;

        try {
            $this->ossClient->putObject($this->aliOss["bucket"], $object, $content);
            $data = array(
                "code" => 1,
                "msg" => "success"
            );
        }
        catch (OssException $e) {
            $data = array(
                "code" => 0,
                "msg" => $e->getMessage()
            );
        }

        return $data;
    }




    /*
     *阿里oss删除文件接口
     *@param  string $path        文件路径
     *return  array  $data          返回状态  0-失败 1——失败
     */
    public function delAliFile($path)
    {
        // 填写文件完整路径，例如exampledir/exampleobject.txt。文档完整路径中不能包含Bucket名称。 本地路径        
        $object = $path;

        try {

            $this->ossClient->deleteObject($this->aliOss["bucket"], $object);
            $data = array(
                "code" => 1,
                "msg" => "success"
            );
        }
        catch (OssException $e) {

            $data = array(
                "code" => 0,
                "msg" => $e->getMessage()
            );
        }
        return $data;
    }



}
