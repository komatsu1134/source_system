<?php
namespace app\api\controller;
use think\Controller;
use QL\QueryList;
class Caiji extends Controller
{
    //采集接口   
    public function gather()
    {

    }






    /*
     *采集详情页
     *@param string $url   详情页url
     *@param []     $ruler  定义采集规则  
     *return []     $data   采集的内容组合  
     */
    public function gatherDetailPage($url, $ruler = [])
    {

        $url = isset($url) ? $url : 'https://www.ithome.com/html/discovery/358585.htm';
        // 定义采集规则        
        //默认采集规则
        $default_ruler = [
            // 采集文章标题
            'title' => ['h1', 'text'],
            // 采集文章作者
            'author' => ['#author_baidu>strong', 'text'],
            // 采集文章内容
            'content' => ['.post_content', 'html']
        ];

        $rules = isset($ruler) || empty($ruler) == false ? $ruler : $default_ruler;

        $data = QueryList::get($url)->rules($rules)->encoding('UTF-8')->query()->getData();

        return $data;
    }





    /*
     *采集列表
     *@param string $url   列表页url
     *@param []     $ruler  定义采集规则  
     *return []     $data   采集的列表详情页链接的数组  
     */
    public function gatherList($url, $ruler = [])
    {
        $url = isset($url) ? $url : 'https://www.ithome.com/html/discovery/358585.htm';
        // 定义采集规则        
        //默认采集规则
        $default_ruler = [
            'title' => ['a', 'text'],
            // 采集详情页链接
            'link' => ['a', 'href'],
        ];

        $rules = isset($ruler) || empty($ruler) == false ? $ruler : $default_ruler;

        $linkArray = QueryList::get($url)->rules($rules)->encoding('UTF-8')->query()->getData()->all();

        return $linkArray;
    }
}
