<?php
namespace app\api\controller;
use think\Controller;
use thiagoalessio\TesseractOCR\TesseractOCR;
use think\Db;



class Api extends Controller
{

//后台模板主页
public function api($y)
{
echo $y;
 //接收请求的数据
 //$question=input('post.question');
 echo "<script>alert('测试成功！');</script>";
 exit;
 //判断请求数据
 if(empty($question)){
 $this->error('查询数据不能为空！');
 }
 //连接数据库开始查询
 $data=Db::name('bank1')->where('question',$question)->select();
 //模糊查询
// $data=Db::name('bank1')->where('question','like','%{$question}%')->select();
 echo json_encode($data, JSON_UNESCAPED_UNICODE);  // 不编码中文
 echo '测试成功！';
}


               
     // 文字图像识别

  public function getImgText(){
   
   try {
    
    // 支持网络url图片地址和imgData数据
   if(isset($_GET["url"])){
    // 网络url调用
    // 文件流
   $ch = curl_init ();  
   curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );  
   curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );  
   curl_setopt ( $ch, CURLOPT_URL, $_GET["url"]);  
   ob_start ();  
   curl_exec ( $ch );  
   $content = ob_get_contents ();  
   ob_end_clean (); 
   }else if(isset($_POST["imageData"])){
    // 接受图片数据---读取数据流
    $content = $_POST["imageData"];
   }
   
   
//   echo $content;
   // 文字图像识别 
   $ocr = new TesseractOCR();
   $ocr->imageData($content, strlen($content));
   
   //语言设置
   if(isset($_GET["lang"]) || isset($_POST["lang"]) ){
       
      $lang_array = isset($_GET["lang"])?$_GET["lang"]:$_POST["lang"];
      $lang = explode('+', $lang_array);
      //语言设置   
      $ocr->lang($lang);
   }else{
       
      //默认语言
      $lang = ['eng', 'chi_sim'];
      $ocr->lang($lang);
   }
   
   $data = $ocr->run(); 
   
   $data = array(
      "code" => 200,
      "result" => $data
    );
   }catch(Exception $e){
    //   错误处理
     $data = array(
      "code" => 500,
      "result" => $e->getMessage()
      );
   }
   
   return json_encode($data, JSON_UNESCAPED_UNICODE);
}


   /**
    * 题库搜索接口
    * @return bool|string
    */
   public function quApi()
   {
      $q = input("get.q");

      $data_array = $this->getKeyword($q);

      // dump($data_array);
      $contentStr = "没能查询到！";

      for ($i = count($data_array) - 1; $i >= 0; $i--) {

         // var_dump($data_array[$i]);
         // 循环遍历查找
         $a = Db::name('answer_source')->where('question', 'like', "%" . $data_array[$i] . "%")->find();

         if (!empty($a)) {
            //  广告设置
            $ad = Db::name("wechat_site")->where('id', 1)->find();
            // 跳出循环
            $contentStr = $a;
            break;
         }
      }

      //   错误处理
      $data = array(
         "code" => 200,
         "result" => $contentStr
      );

      return json_encode($data, JSON_UNESCAPED_UNICODE);
   }


   //文本关键词提取函数---返回数组
   private function getKeyword($text)
   {

      if (preg_match_all('/[\x{4e00}-\x{9fa5}]+/u', $text, $matches1)) {

         preg_match_all('/[^0-9|,|、][\x{4e00}-\x{9fa5}a-zA-Z0-9_、，]+/u', $text, $matches);
         // // 存在中文
         // dump($matches);

         $data = $this->maopao($matches[0]);
      }
      else {
         // 其他字符
         preg_match_all('/[^0-9|、|,|\s][A-Za-z0-9_ ]+/u', $text, $matches); //这里有空格，注意
         //  dump($matches);

         $data = $this->maopao($matches[0]);
      }
      return $data;
   }



   //   冒泡算法
   private function maopao($arr)
   {

      $len = count($arr);

      for ($k = 0; $k <= $len; $k++) {

         for ($j = $len - 1; $j > $k; $j--) {

            if (strlen($arr[$j]) < strlen($arr[$j - 1])) {

               $temp = $arr[$j];

               $arr[$j] = $arr[$j - 1];

               $arr[$j - 1] = $temp;

            }

         }

      }
      return $arr;
   }

}


