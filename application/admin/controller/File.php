<?php
namespace app\admin\controller;

use think\Controller;
use ZipArchive;

// 文件操作类

class File extends Controller{

     
    // 下载文件到指定目录解压
    private function downloadFileToDir($url){
        
         // 文件流
         $ch = curl_init ();  
         curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );  
         curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );  
         curl_setopt ( $ch, CURLOPT_URL, $url );  
         ob_start ();  
         curl_exec ( $ch );  
         $return_content = ob_get_contents ();  
         ob_end_clean ();  
         $return_code = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );  
         return $return_content;  
    }
    
    // 模版自动下载脚本操作
    public function downloadTempalte(){

        // 参数接受
        $url = input("get.url");
        
        // 指定目录
        $dir = $_SERVER['DOCUMENT_ROOT']."/template/";

        $content = $this->downloadFileToDir($url);

        // dump($content);

        // 文件名获取
        $fileArray = explode("/", $url);

        $end = $fileArray[count($fileArray) - 1];
        
        // 指定解压下载目录
        $fileDir = $dir.explode(".", $end)[0];

        if(!is_dir($dir)){    
            mkdir($dir, 0777, true);
        }

            // 临时文件
            $filename = $dir.$end;
            // 保存文件解压到template目录
            // if(!is_dir($filename)){
            //     mkdir($filename, 0777, true);
            // }

            $fp= fopen($filename,"a"); //将文件绑定到流
            fwrite($fp,$content); //写入文件
            fclose($fp);
            // 解压文件 
            $file = new ZipArchive();
            if($file->open($filename)){
               $file->extractTo($fileDir);
               $file->close();

              //解压成功-删除文件
              if(unlink($filename)){
                   $data = array(
                    "code" => 200,
                    "msg" => "模版下载成功！"
                   );
              return json_encode($data, JSON_UNESCAPED_UNICODE);
              }
            }  
    }



}