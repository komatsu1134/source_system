<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;



class Api extends Controller
{

    // 请求第三方api配置信息表接口
    public function getThirdApi()
    {

        $data = Db::name("api")->find();

        $re = array(
            "code" => 200,
            "description" => "这是第三方接口配置信息",
            "data" => $data
        );
        return json_encode($re, JSON_UNESCAPED_UNICODE);
    }


    public function setThirdApi()
    {
        $data = $_POST;
        $re = Db::name("api")->where("id", $data["id"])->update($data);
        if (!empty($re)) {
            $re = array(
                "code" => 200,
                "msg" => "修改成功！"
            );
        }
        else {
            $re = array(
                "code" => 300,
                "msg" => "修改失败"
            );
        }
        return json_encode($re, JSON_UNESCAPED_UNICODE);
    }



}