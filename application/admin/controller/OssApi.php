<?php
namespace app\admin\controller;

use think\Db;
use think\Controller;

class OssApi extends Controller
{


    //获取oss配置信息接口
    public function getOss()
    {
        $data = Db::name('oss')->find();
        $re = array(
            "code" => 200,
            'description' => "这是oss配置信息接口",
            "data" => $data
        );

        return json_encode($re, JSON_UNESCAPED_UNICODE);
    }


    /*
     *修改oss配置信息接口
     *@param array   POST
     *@return []
     */
    public function setOss()
    {
        //获取
        $data = file_get_contents('php://input');
        $data = (array)json_decode($data); //转化为数组

        // dump($data);
        $re = Db::name('oss')->where('id', $data["id"])->update($data);

        if (!empty($re)) {
            //ok
            $res = array(
                "code" => 200,
                "msg" => "修改oss成功"
            );
        }
        else {
            //fail
            $res = array(
                "code" => 500,
                "msg" => "修改oss失败"
            );
        }

        return json_encode($res, JSON_UNESCAPED_UNICODE);
    }
}
