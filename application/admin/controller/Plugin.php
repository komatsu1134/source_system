<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;

class Plugin extends Controller
{


    // 获取已安装的插件列表
    public function getPlugins()
    {
        $dir = $_SERVER['DOCUMENT_ROOT'] . "/plugin/";
        $dir1 = scandir($dir);

        $j = 0;
        $temList = [];
        foreach ($dir1 as $key => $temp) {
            if (!preg_match('/^\..*/', $temp) && is_dir($dir . $temp)) {

                // 插件
                $temList[$j] = array(
                    "index" => "plugin",
                    "title" => file_get_contents($dir . $temp . "/name.txt"),
                    "url" => "http://" . $_SERVER["HTTP_HOST"] . "/plugin/" . $temp,
                );
                $j++;
            }
        }
        $data = array(
            "code" => 200,
            "description" => "这是插件列表信息",
            "data" => $temList
        );

        // dump($temList);
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }


}